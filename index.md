# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

Semestre 2019-1

### Presentación del curso

+ [Presentación del curso](presentacion.md "Generalidades del curso")

### Prácticas

+ [Índice de prácticas](http://SistemasOperativos-Ciencias-UNAM.gitlab.io/practicas/ "Índice de prácticas")
+ [Especificaciones de las prácticas en formato PDF](/public/practicas "Archivos PDF de las prácticas")

### Temas

+ [Guía rápida de `git`](temas/git.md "9418/tcp")
+ [Arquitectura y componentes de computadoras](temas/arch.md "arch.md")

### Tareas

+ [Arquitectura y componentes de una computadora](tareas/tarea-arch.md)

### Ligas de interés

+ <http://SistemasOperativos-Ciencias-UNAM.gitlab.io/>
+ <https://tinyurl.com/ListaSO-2019-1>
+ <https://groups.google.com/a/ciencias.unam.mx/group/sistemasoperativos-alumnos/>
+ <http://www.fciencias.unam.mx/asignaturas/713.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/presentacion/295737>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/295737>
+ <http://www.fciencias.unam.mx/docencia/horarios/20191/218/713>
+ <http://www.fciencias.unam.mx/docencia/horarios/20191/1556/713>
