# Tipos de archivo en sistemas UNIX

+ <https://en.wikipedia.org/wiki/Unix_file_types>
+ <http://www.tldp.org/LDP/intro-linux/html/sect_03_01.html>
+ <https://www.linux.com/blog/file-types-linuxunix-explained-detail>
+ <https://www.unixtutorial.org/2007/09/unix-file-types/>
+ <https://www.linuxnix.com/file-types-in-linux/>

## 1 - Archivo normal

## 2 - Directorio

## 3 - Liga simbólica

### Liga dura

## 4 - FIFO - Named Pipe

## 5 - UNIX Domain Socket

## Dispositivos

### 6 - Dispositivo de bloques

### 7 - Dispositivo de caracteres
