# Tipos de _kernel_

+ <https://wiki.osdev.org/Kernel>
+ <https://www.slideserve.com/isabelle-hopper/microkernels-virtualization-exokernels>
+ <https://techdifferences.com/difference-between-microkernel-and-monolithic-kernel.html>
+ <https://www.slideshare.net/hajimetazaki/linux-kernel-library-reusing-monolithic-kernel>

## Kernel monolítico

+ <https://wiki.osdev.org/Monolithic_kernel>

![Kernel monolítico](https://wiki.osdev.org/images/a/aa/Monolithic.png "Kernel monolítico")

## Microkernel

+ <https://wiki.osdev.org/Microkernel>

![MicroKernel](https://wiki.osdev.org/images/2/28/Microkernel.png "MicroKernel")

## Kernel modular

+ <https://wiki.osdev.org/Modular_Kernel>

## Diferencias

Kernel monolítico vs microkernel

![](https://image3.slideserve.com/7020606/monolithic-kernel-vs-microkernel-n.jpg "")
![](https://techdifferences.com/wp-content/uploads/2016/12/Microkernel-Vs-Monolithic-Kernel.jpg "")
![](https://i.ytimg.com/vi/4RVSgxrsxvE/maxresdefault.jpg "")

## Componentes del kernel

### Linux

+ <https://commons.wikimedia.org/wiki/File:Linux_kernel_diagram.png>
+ <https://www.tldp.org/LDP/sag/html/kernel-parts.html>
+ <https://www.linux-india.org/characteristics-and-architecture-of-linux-oprating-system/>
+ <http://www.mnis.fr/ocera_support/architecture/x531.html>
+ <http://www.dba-oracle.com/t_otn_7_linux_kernel.htm>
+ <https://www.ibm.com/developerworks/library/l-linuxuniversal/>

### Windows

+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/kernel/overview-of-windows-components>
+ <https://blogs.msdn.microsoft.com/hanybarakat/2007/02/25/deeper-into-windows-architecture/>

## Kernel space y User space

+ <https://drawings.jvns.ca/userspace/>

![](https://drawings.jvns.ca/drawings/userspace.png "")

+ <https://caitlincomputerscience.wordpress.com/2015/10/15/kernel-space-vs-user-space/>
+ <https://www.slideshare.net/sureskal/linux-sk-v01>
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/user-mode-and-kernel-mode>

## Llamadas al sistema

+ <https://en.wikipedia.org/wiki/System_call>
