# Arquitectura y componentes de computadoras

## POST - Power On Self Test

+ <https://wiki.osdev.org/System_Initialization_(x86)>
+ <https://en.wikipedia.org/wiki/Power-on_self-test>
+ <https://en.wikipedia.org/wiki/POST_card>

## BIOS y UEFI

+ <https://wiki.osdev.org/BIOS>
+ <https://wiki.osdev.org/UEFI>
+ <https://members.uefi.org/learning_center/papers/>

## Inicio (boot)

+ <https://en.wikipedia.org/wiki/Booting>
+ <https://wiki.osdev.org/Bootloader>
+ <https://wiki.osdev.org/Rolling_Your_Own_Bootloader>
+ <https://wiki.osdev.org/MBR_(x86)>
+ <https://manybutfinite.com/post/how-computers-boot-up/>
+ <http://www.drdobbs.com/parallel/booting-an-intel-architecture-system-par/232300699?pgno=1>
+ <http://www.drdobbs.com/parallel/booting-an-intel-architecture-system-par/232300699?pgno=2>
+ <http://www.drdobbs.com/parallel/booting-an-intel-architecture-system-par/232300699?pgno=3>
+ <http://www.drdobbs.com/parallel/booting-an-intel-architecture-system-par/232300699?pgno=4>
+ <http://lxr.linux.no/>
+ <https://en.wikipedia.org/wiki/Linux_startup_process>

## Chipset

+ <https://manybutfinite.com/post/motherboard-chipsets-memory-map/>

--------------------------------------------------------------------------------

## Videos complementarios

History of the Motherboard
+ <https://www.youtube.com/watch?v=Mw6ha5wxJUI>

32-bit vs 64-bit Computers & Phones as Fast As Possible
+ <https://www.youtube.com/watch?v=IknbgnJLSRY>

Coding Communication & CPU Microarchitectures as Fast As Possible
+ <https://www.youtube.com/watch?v=FkeRMQzD-0Y>

BIOS and UEFI as Fast As Possible
+ <https://www.youtube.com/watch?v=zIYkol851dU>

CPU Sockets as Fast As Possible
+ <https://www.youtube.com/watch?v=rShVLBIR2VA>

RAM Speed and Timings As Fast As Possible
+ <https://www.youtube.com/watch?v=h-TWQ0rS-SI>

DDR Memory vs GDDR Memory as Fast As Possible
+ <https://www.youtube.com/watch?v=pbgvzVgfoSc>

PCI Express 4.0 as Fast As Possible
+ <https://www.youtube.com/watch?v=aXMJeozEl4A>

SSDs vs Hard Drives as Fast As Possible
+ <https://www.youtube.com/watch?v=YQEjGKYXjw8>

Raspberry Pi as Fast As Possible
+ <https://www.youtube.com/watch?v=5jA8wYqQLBU>

ARM CPUs as Fast As Possible
+ <https://www.youtube.com/watch?v=X4BxUiqWq8E>

Systems on a Chip (SOCs) as Fast As Possible
+ <https://www.youtube.com/watch?v=L4XemL7t6hg>

Motherboard VRMs As Fast As Possible
+ <https://www.youtube.com/watch?v=KRRODHoQAHI>
