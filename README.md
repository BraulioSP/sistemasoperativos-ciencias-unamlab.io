# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/>

Semestre 2019-1

### Presentación del curso

+ [Presentación del curso](presentacion.md "Generalidades del curso")

### Prácticas

+ [Índice de prácticas](https://SistemasOperativos-Ciencias-UNAM.gitlab.io/practicas/ "Índice de prácticas")
+ [Especificaciones de las prácticas en formato PDF](/public/practicas "Archivos PDF de las prácticas")

### Temas

+ [Guía rápida de `git`](git.md "9418/tcp")
+ [Arquitectura y componentes de computadoras](temas/arch.md "arch.md")
+ [Tipos de _kernel_](temas/kernel.md "kernel.md")
+ [Tipos de archivo en UNIX](temas/filetypes.md "filetypes.md")
+ [Programas y procesos](temas/ps-proc.md "ps-proc.md")

### Tareas

+ [Arquitectura y componentes de una computadora](tareas/tarea-arch.md)
+ [Programas de sistema de archivos, procesos, hilos, señales y sockets](tareas/programas.md)
+ [Módulos de _kernel_](tareas/linux-module.md)

### Ligas de interés

+ <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/>
+ <https://tinyurl.com/ListaSO-2019-1>
+ <https://groups.google.com/a/ciencias.unam.mx/group/sistemasoperativos-alumnos/>
+ <http://www.fciencias.unam.mx/asignaturas/713.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/presentacion/295737>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/295737>
+ <http://www.fciencias.unam.mx/docencia/horarios/20191/218/713>
+ <http://www.fciencias.unam.mx/docencia/horarios/20191/1556/713>
